# Membres
	- Luc LIGER
	- Joffrey DALENCON


# Lien dépot git

## HTTPS:
git clone https://liger3u@bitbucket.org/liger3u/lpcisiie_racoin_dalencon_liger.git

## SSH:
git clone git@bitbucket.org:liger3u/lpcisiie_racoin_dalencon_liger.git

# Lien webetu 
https://webetu.iutnc.univ-lorraine.fr/~liger3u/projet/index.html

# Scénarios
# Josiane
	* Faire une recherche en combinant une catégorie précise, des mots clés,
	* ordonner le résultat par date, le plus récent en premier,
	* visualiser rapidement le résultat en faisant défiler l'ensemble des annonces avec les 
	informations essentielles,
	* cliquer sur une annonce intéressante et obtenir les détails de l'annonce. 
	* visualiser les différentes photos de l'objet pour vérifier son état et son intérêt
# Louis
	* s'identifier/s'authentifier grace à mon compte "pro",
	* visualiser la liste de mes annonces pour supprimer celles qui correspondent à des objets 
		vendus, ou modifier des annonces correspondant à des objets non vendus.
	* ajouter une ou plusieurs annonces. Toutes mes données personnelles sont déjà saisies (en 
		particulier, tout ce qui est nécessaire pour me contacter). Je peux saisir le texte descriptif de 
		l'annonce, le prix, et choisir la catégorie la mieux adaptée. 
	* Je peux visualiser mon annonce avant de la valider définitivement.
	* Je peux mettre plusieurs photos pour une même annonce
# Sylvain
	* sur le site, je navigue dans la liste des annonces récentes concernant quelques catégories, et 
		éventuellement, si une annonce m'attire, je clique pour obtenir plus d'info.
	* si une annonce m'intéresse, en général je la garde dans mes bookmarks pour y revenir par la 
		suite.
	* de temps en temps, je publie une annonce. Je le fait sans créer de compte. Je saisis 
		l'ensemble des informations décrivant l'annonce, ainsi que mes coordonnées pour le 
		contacter, et un mot de passe pour modifier/supprimer cette annonce.
	* je peux visualiser toutes mes annonces en indiquant mon adresse mail. A partir de là, je peux
		modifier ces annonces à condition de fournir le mot de passe que je lui ai associé à la 
		création. En général je ne le fait pas car j'ai oublié ce mot de passe.